"use strict";

app.controller('EditUserController', ['$scope', 'user', '$stateParams', 'UserService', '$rootScope', function ($scope, user, $stateParams, UserService, $rootScope) 
{

	$scope.formType = 'Edit';

	$scope.submitMessage = 'Save Changes';

	$scope.user = user;

	$scope.formSubmit = function()
	{

		var editUser = UserService.editUser({id: $stateParams.id, params: $scope.user });

		editUser.then(function(response) {

			$rootScope.$broadcast('App::message', { type: 'success', message: response });

		}, function(error)
		{

			$rootScope.$broadcast('App::message', { type: 'danger', message: response });

		});

	}	

}])