"use strict";

app.controller('ListUserController', ['$scope', '$rootScope', 'UserService', '$timeout', function ($scope, $rootScope, UserService, $timeout) 
{
	$scope.deleteUser = function(id)
	{
		// Clear any messages from any previous form submissions
		$rootScope.$broadcast('App::message', { type: 'clear' });

		var deleteUser = UserService.deleteUser(id);

		deleteUser.then(function(response) {

			$rootScope.$broadcast('App::message', { type: 'success', message: response });

			$timeout(function() {
				$scope.$apply();
			}, 5);

		}, function(error)
		{

			$rootScope.$broadcast('App::message', { type: 'danger', message: response });

		});
	}	
}])