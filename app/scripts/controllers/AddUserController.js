"use strict";

app.controller('AddUserController', ['$scope', 'UserService', '$rootScope', function ($scope, UserService, $rootScope) 
{

	$scope.formType = 'Add';

	$scope.submitMessage = 'Add User';


	$scope.formSubmit = function()
	{
		// Clear any messages from any previous form submissions
		$rootScope.$broadcast('App::message', { type: 'clear' });

		var addUser = UserService.addUser({ firstName: $scope.user.firstName, lastName: $scope.user.lastName });

		addUser.then(function(response) {

			$rootScope.$broadcast('App::message', { type: 'success', message: response });

		}, function(error)
		{

			$rootScope.$broadcast('App::message', { type: 'danger', message: response });

		});

	}
	
}])