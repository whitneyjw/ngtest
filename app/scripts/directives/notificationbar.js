"use strict";

app.directive('notificationbar', ['$rootScope', function ($rootScope) 
{
	return {
		restrict: 'AE',
		link: function (scope, element, attributes) 
		{
			scope.alerts = [];

			$rootScope.$on('App::message', function(event, notification) {
				if ( notification.type == 'clear' )
				{
					scope.alerts = [];
				}
				else {
					var alert = { type: notification.type, message: notification.message};
					scope.alerts.push(alert);
				}
			})

			scope.$on('$stateChangeSuccess', function() {
				scope.alerts = [];
			})

			scope.closeAlert = function(index)
			{
				scope.alerts.splice(index, 1);
			}
		}
	};
}])