"use strict";

app.factory('UserService', ['$q', '$timeout', function($q, $timeout) {

	var Users = [];

	var UserService = {

		// Returns an object of all users
		getAllUsers: function()
		{
			var deferred = $q.defer();

			$timeout(function() 
			{
				return deferred.resolve(Users);
			}, 250);

			return deferred.promise;
		},

		// Adds user to list
		addUser: function(user)
		{
			var deferred = $q.defer();

			$timeout(function() 
			{
				Users.push(user);
				return deferred.resolve('User successfully added');
			}, 250);

			return deferred.promise;
			
		},

		// Removes a user from the list
		deleteUser: function(id)
		{

			var deferred = $q.defer();

			$timeout(function() 
			{
				Users.splice(id, 1);
				return deferred.resolve('User successfully deleted');
			}, 250);

			return deferred.promise;
		},

		find: function(id)
		{
			var deferred = $q.defer();

			$timeout(function() 
			{
				if ( Users[id] !== undefined )
					return deferred.resolve(Users[id]);
				else
					return deferred.reject('Specified user does not exist!');
			}, 250);

			return deferred.promise;
		},

		editUser: function(obj)
		{
			var deferred = $q.defer();

			$timeout(function() 
			{
				if ( Users[obj.id] !== undefined )
				{
					Users[obj.id] = obj.params;
					return deferred.resolve('Changes have been saved.');
				}
				else {
					return deferred.reject('Specified user does not exist!');
				}
			}, 250);

			return deferred.promise;
		}
	}

	return UserService;

}])