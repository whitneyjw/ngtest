'use strict';

var app = angular.module('ngtestApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ui.bootstrap',
    'ui.router'
  ]);

app.run(['$state', '$rootScope', function($state, $rootScope)
{
  $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error)
  {
    $rootScope.$broadcast('App::message', { type: 'danger', message: error });
  })
}])
