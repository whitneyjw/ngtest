"use strict";

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) 
{
    
	// Assign states
    $stateProvider
    	.state('user', {
    		abstract: true,
    		url: '/user',
    		templateUrl: 'views/main.tpl.html'
    	})
	    	// List state
	    	.state('user.list', {
	    		url: '/list',
	    		templateUrl: 'views/list.tpl.html',
	    		resolve: {
	    			Users: ['UserService', function(UserService) 
	    			{
	    				return UserService.getAllUsers();
	    			}]
	    		},
	    		controller: ['$scope', 'Users', function($scope, Users)
	    		{
	    			$scope.Users = Users;
	    		}]
	    	})

	    	// Add State
	    	.state('user.add', {
	    		url: '/add',
	    		templateUrl: 'views/user.tpl.html',
	    		controller: 'AddUserController'
	    	})

	    	// Edit State
	    	.state('user.edit', {
	    		url: '/edit/:id',
	    		templateUrl: 'views/user.tpl.html',
	    		controller: 'EditUserController',
	    		resolve: {
	    			user: ['$stateParams', 'UserService', function($stateParams, UserService) {
	    				return UserService.find($stateParams.id);
	    			}]
	    		}
	    	});

    $urlRouterProvider.otherwise("/user/list");

}]);